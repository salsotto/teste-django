# O que usar: #

* Django 2.2.9
* PostgreSQL 11
* Pipenv (pipfile)

# O que não usar: #

* django-admin

# O que fazer: #

* Criar a modelagem relacional a partir da planilha (baixe neste link: encurtador.com.br/EKR29)
* Normalizar os dados da maneira que achar adequada (via código)
* Criar um importador que faça o processamento da planilha (salvando os dados de acordo com a modelagem que criou)
* Faça as validações que achar necessário
* Faça o processamento da planilha ocorrer em segundo plano (usando celery e redis)
* Criar um endpoint (django-rest) dos dados importados
* Faça a autenticação do endpoint que retorne um token para acesso
* Uma vez logado, faça uma paginação para disponibilizar a listagem de demandas cadastradas
* Criar um template para apresentar as demandas cadastradas
* Criar uma tabela usando bootstrap que apresente os dados
* Criar fixture das categorias

# Como entregar: #

Criar um repositório no Bitbucket e compartilhar com:

* caue.martines.prs@synergiaconsultoria.com.br 
* alexsandro.rocha.prs@synergiaconsultoria.com.br 
* vinicius.salsotto@synergiaconsultoria.com.br


# Bônus: #

* Subir o projeto com docker (dockerfile / docker-compose)